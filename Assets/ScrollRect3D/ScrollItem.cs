﻿using UnityEngine;
using System.Collections;

public class ScrollItem : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider col)
    {
        if (col.name.Equals("Left"))
        {
            Transform last = transform.parent.GetChild(8);
            transform.localPosition = last.localPosition;
            transform.Translate(2.5f, 0f, 0f);
            transform.SetAsLastSibling();
        }
        else if (col.name.Equals("Right"))
        {
            Transform first = transform.parent.GetChild(0);
            transform.localPosition = first.localPosition;
            transform.Translate(-2.5f, 0f, 0f);
            transform.SetAsFirstSibling();
        }

    }
}
